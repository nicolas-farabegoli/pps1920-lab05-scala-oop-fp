package u05lab.code

import java.util.concurrent.TimeUnit
import java.util.stream.{Collectors, DoubleStream, IntStream}

import scala.collection.{immutable, mutable}
import scala.concurrent.duration.FiniteDuration
import scala.util.Random

object PerformanceUtils {
  case class MeasurementResults[T](result: T, duration: FiniteDuration) extends Ordered[MeasurementResults[_]] {
    override def compare(that: MeasurementResults[_]): Int = duration.toNanos.compareTo(that.duration.toNanos)
  }

  def measure[T](msg: String)(expr: => T): MeasurementResults[T] = {
    val startTime = System.nanoTime()
    val res = expr
    val duration = FiniteDuration(System.nanoTime()-startTime, TimeUnit.NANOSECONDS)
    if(!msg.isEmpty) println(msg + " -- " + duration.toNanos + " nanos; " + duration.toMillis + "ms")
    MeasurementResults(res, duration)
  }

  def measure[T](expr: => T): MeasurementResults[T] = measure("")(expr)
}


object CollectionsTest extends App {

  /* Linear sequences: List, ListBuffer */
  val immList: immutable.List[Int] = (1 to 1_000_000).toList
  val mutList: mutable.ListBuffer[Int] = mutable.ListBuffer()
  immList.foreach(mutList += _) // Copy the content of immList to mutList

  /* Indexed sequences: Vector, Array, ArrayBuffer */
  val vect: Vector[Int] = (0 to 1_000_000).toVector
  val arr: Array[Int] = (0 to 1_000_000).toArray
  val arrBuf: mutable.ArrayBuffer[Int] = mutable.ArrayBuffer()
  arr.foreach(arrBuf += _)

  /* Sets */
  val set: Set[Int] = (1 to 1_000_000).toSet
  val mutSet: mutable.Set[Int] = mutable.Set()
  mutSet.addAll(set)

  /* Maps */
  val map: Map[Int, String] = (1 to 1_000_000).map(e => e -> e.toString).toMap
  val mutMap: mutable.Map[Int, String] = mutable.Map()
  mutMap.addAll(map)

  /* Comparison */
  import PerformanceUtils._
  val lst = (1 to 1000000).toList
  val vec = (1 to 1000000).toVector
  assert( measure("lst last"){ lst.last } > measure("vec last"){ vec.last } )

  // Test List and ListBuffer
  measure("Add elements to List"){ 2 :: 3 :: 4 :: immList }
  measure("Add elements to ListBuffer") { mutList += 2 += 3 += 4 }
  measure("Modify element to List") { immList.updated(3, 5)}
  measure("Modify Element to ListBuffer"){ mutList.update(3, 5) }
  measure("Remove from List"){ immList.filter(_ > 500_000) }
  measure("Remove from ListBuffer"){ mutList.remove(500_000, 500_000) }

  // Test on Indexed Sequence
  measure("Read element Vector"){ vec(4567) }
  measure("Read element Array"){ arr(4567) }
  measure("Read element ArrayBuffer"){ arrBuf(4567) }

  measure("Add element to Vector"){ vect.appendedAll((2 to 5).toList) }
  measure("Add element to Array"){ arr.appendedAll((2 to 5).toList) }
  measure("Add element to ArrayBuffer"){ arrBuf.appendedAll((2 to 5).toList) }

  measure("Remove element to Vector"){ vect.filter(x => x <= 6) }
  measure("Remove element to Array"){ arr.filter(x => x <= 6) }
  measure("Remove element to ArrayBuffer"){ arrBuf -= (0, 1, 2, 3, 4, 5, 6) }

  // Test on Set
  measure("Add to Set"){ set ++ Set(20, 30, 40) }
  measure("Add to mutable.Set"){ mutSet ++= Set(20, 30, 40) }
  measure("Remove from Set"){ set -- Set(20, 21, 22) }
  measure("Remove from mutable.Set"){ mutSet --= Set(21, 22, 23) }

  // Test on Map
  measure("Add top Map"){ map ++ Map(-3 -> "-3", -4 -> "-4") }
  measure("Add to mutable.Map"){ mutMap ++= Map(-3 -> "-3", -4 -> "-4")}
  measure("Remove from Map"){ map - 2 - 3 - 4 - 5 }
  measure("Remove from mutable.Map"){ mutMap -= (2, 3, 4, 5) }
}