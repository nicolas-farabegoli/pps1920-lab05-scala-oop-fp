package u05lab.code

import scala.collection.mutable

object ExamsManagerTest extends App {

  /* See: https://bitbucket.org/mviroli/oop2018-esami/src/master/a01b/e1/Test.java */

  object Kind extends Enumeration { val RETIRED, FAILED, SUCCEEDED = Value }

  trait ExamResult {
    def kind: Kind.Value
    def evaluation: Option[Int]
    def cumLaude: Boolean
  }

  object ExamResult {
    def apply(kind: Kind.Value, evaluation: Option[Int], cumLaude: Boolean): ExamResult = ExamResultImpl(kind, evaluation, cumLaude)
    private case class ExamResultImpl(override val kind: Kind.Value, override val evaluation: Option[Int], override val cumLaude: Boolean) extends ExamResult {
      override def toString: String = kind match {
        case Kind.SUCCEEDED if (cumLaude) => s"SUCCEEDED(30L)"
        case Kind.SUCCEEDED => s"SUCCEEDED(${evaluation.get})"
        case Kind.RETIRED => s"RETIRED"
        case Kind.FAILED => s"FAILED"
      }
    }
  }

  trait ExamResultFactory {
    def failed(): ExamResult
    def retired(): ExamResult
    def succeededCumLaude(): ExamResult
    def succeeded(evaluation: Int): ExamResult
  }

  object ExamResultFactory {
    def apply(): ExamResultFactory = ExamResultFactoryImpl()
    private case class ExamResultFactoryImpl() extends ExamResultFactory {
      override def failed(): ExamResult = ExamResult(Kind.FAILED, Option.empty, cumLaude = false)
      override def retired(): ExamResult = ExamResult(Kind.RETIRED, Option.empty, cumLaude = false)
      override def succeededCumLaude(): ExamResult = ExamResult(Kind.SUCCEEDED, Option.apply(30), cumLaude = true)
      override def succeeded(evaluation: Int): ExamResult = {
        if (evaluation > 30 || evaluation < 18) {
          throw new IllegalArgumentException()
        }
        ExamResult(Kind.SUCCEEDED, Option.apply(evaluation), cumLaude = false)
      }
    }
  }

  trait ExamsManager {
    def createNewCall(call: String): Unit
    def addStudentResult(call: String, student: String, result: ExamResult): Unit
    def getAllStudentFromCall(call: String): Set[String]
    def getEvaluationsMapFromCall(call: String): Map[String, Int]
    def getResultsMapFromStudent(student: String): Map[String, String]
    def getBestResultFromStudent(student: String): Option[Int]
  }

  object ExamsManager {
    def apply(): ExamsManager = ExamsManagerImpl()
    private case class ExamsManagerImpl() extends ExamsManager {

      private var calls: Map[String, mutable.Map[String, ExamResult]] = Map()

      override def createNewCall(call: String): Unit = {
        if (calls.contains(call)) throw new IllegalArgumentException()
        calls += (call -> mutable.Map[String, ExamResult]())
      }

      override def addStudentResult(call: String, student: String, result: ExamResult): Unit = {
        if (calls(call).contains(student)) throw new IllegalArgumentException()
        calls(call) += (student -> result)
      }

      override def getAllStudentFromCall(call: String): Set[String] = calls(call).keySet.toSet

      override def getEvaluationsMapFromCall(call: String): Map[String, Int] =
        calls(call)
          .filter(e => e._2.kind == Kind.SUCCEEDED)
          .map(e => e._1 -> e._2.evaluation.get).toMap

      override def getResultsMapFromStudent(student: String): Map[String, String] =
        calls.filter(e => e._2.contains(student))
          .map(e => e._1 -> e._2(student).toString)


      override def getBestResultFromStudent(student: String): Option[Int] =
        calls.flatMap(e => e._2)
        .filter(e => e._1 == student)
        .map(e => e._2.evaluation)
        .max
    }
  }

}