package u05lab.code

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._
import u05lab.code.ExamsManagerTest.{ExamResult, ExamResultFactory, ExamsManager, Kind}


class ExamManagerTest {

  @Test
  def testExamResultBasicBehaviour(): Unit = {
    val erf = ExamResultFactory()
    assertEquals(erf.failed().kind, Kind.FAILED)
    assertFalse(erf.failed().evaluation.isDefined)
    assertFalse(erf.failed().cumLaude)

    assertEquals(erf.retired().kind, Kind.RETIRED)
    assertFalse(erf.retired().evaluation.isDefined)
    assertFalse(erf.retired().cumLaude)

    assertEquals(erf.succeededCumLaude().kind, Kind.SUCCEEDED)
    assertEquals(erf.succeededCumLaude().evaluation, Option.apply(30))
    assertTrue(erf.succeededCumLaude().cumLaude)

    assertEquals(erf.succeeded(28).kind, Kind.SUCCEEDED)
    assertEquals(erf.succeeded(28).evaluation, Option.apply(28))
    assertFalse(erf.succeeded(28).cumLaude)
  }

  @Test
  def optionalTestEvaluationCantBeGraterThan30(): Unit = {
    val erf = ExamResultFactory()
    assertThrows(classOf[IllegalArgumentException], () => erf.succeeded(32))
  }

  @Test
  def optionalTestEvaluationCantBeSmallerThan18(): Unit = {
    val erf = ExamResultFactory()
    assertThrows(classOf[IllegalArgumentException], () => erf.succeeded(17))
  }

  private def prepareExam(em: ExamsManager, erf: ExamResultFactory): Unit = {
    em.createNewCall("gennaio")
    em.createNewCall("febbraio")
    em.createNewCall("marzo")

    em.addStudentResult("gennaio", "rossi", erf.failed())
    em.addStudentResult("gennaio", "bianchi", erf.retired())
    em.addStudentResult("gennaio", "verdi", erf.succeeded(28))
    em.addStudentResult("gennaio", "neri", erf.succeededCumLaude())

    em.addStudentResult("febbraio", "rossi", erf.failed())
    em.addStudentResult("febbraio", "bianchi", erf.succeeded(20))
    em.addStudentResult("febbraio", "verdi", erf.succeeded(30))

    em.addStudentResult("marzo", "rossi", erf.succeeded(25))
    em.addStudentResult("marzo", "bianchi", erf.succeeded(25))
    em.addStudentResult("marzo", "viola", erf.failed())
  }

  @Test
  def testExamsManagement(): Unit = {
    val em = ExamsManager()
    val erf = ExamResultFactory()

    prepareExam(em, erf)

    assertEquals(em.getEvaluationsMapFromCall("gennaio").size, 2)
    assertEquals(em.getEvaluationsMapFromCall("gennaio")("verdi"), 28)
    assertEquals(em.getEvaluationsMapFromCall("gennaio")("neri"), 30)

    assertEquals(em.getEvaluationsMapFromCall("febbraio").size, 2)
    assertEquals(em.getEvaluationsMapFromCall("febbraio")("bianchi"), 20)
    assertEquals(em.getEvaluationsMapFromCall("febbraio")("verdi"), 30)

    // tutti i risultati di rossi (attenzione ai toString!!)
    assertEquals(em.getResultsMapFromStudent("rossi").size, 3)
    assertEquals(em.getResultsMapFromStudent("rossi")("gennaio"), "FAILED")
    assertEquals(em.getResultsMapFromStudent("rossi")("febbraio"), "FAILED")
    assertEquals(em.getResultsMapFromStudent("rossi")("marzo"), "SUCCEEDED(25)")
    // tutti i risultati di bianchi
    assertEquals(em.getResultsMapFromStudent("bianchi").size, 3)
    assertEquals(em.getResultsMapFromStudent("bianchi")("gennaio"), "RETIRED")
    assertEquals(em.getResultsMapFromStudent("bianchi")("febbraio"), "SUCCEEDED(20)")
    assertEquals(em.getResultsMapFromStudent("bianchi")("marzo"), "SUCCEEDED(25)")
    // tutti i risultati di neri
    assertEquals(em.getResultsMapFromStudent("neri").size, 1)
    assertEquals(em.getResultsMapFromStudent("neri")("gennaio"), "SUCCEEDED(30L)")
  }

  @Test
  def optionalTestExamsManagement(): Unit = {
    val em = ExamsManager()
    val erf = ExamResultFactory()

    prepareExam(em, erf)
    // miglior voto acquisito da ogni studente, o vuoto..
    assertEquals(em.getBestResultFromStudent("rossi"), Option.apply(25))
    assertEquals(em.getBestResultFromStudent("bianchi"), Option.apply(25))
    assertEquals(em.getBestResultFromStudent("neri"), Option.apply(30))
    assertEquals(em.getBestResultFromStudent("viola"), Option.empty)
  }

  @Test
  def optionalTestCantCreateACallTwice(): Unit = {
    val em = ExamsManager()
    val erf = ExamResultFactory()

    prepareExam(em, erf)
    assertThrows(classOf[IllegalArgumentException], () => em.createNewCall("marzo"))
  }

  @Test
  def optionalTestCantRegisterAnEvaluationTwice(): Unit = {
    val em = ExamsManager()
    val erf = ExamResultFactory()

    prepareExam(em, erf)
    assertThrows(classOf[IllegalArgumentException], () => em.addStudentResult("gennaio", "verdi", erf.failed()))
  }

}
